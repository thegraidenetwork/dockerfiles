# The Graide Network - Dockerfiles

This repository contains Dockerfiles we use to create base images for development.

## Dockerfiles

### PHPUnit
This Dockerfile creates a PHPUnit image with PHP-FPM, MySQL, and a large number of extensions and add-ons we use.

#### Use

```bash
docker pull thegraidenetwork/laravel-php[:tag]
```

#### Tags
- `8.0` - The latest, Built on PHP 8.0.

- `7.3` - Built on PHP 7.3.


### PHPUnit
This Dockerfile extends the laravel-php Docker image by adding PHPUnit and XDebug.

#### Use

```bash
docker pull thegraidenetwork/phpunit[:tag]
```

#### Tags
- `latest` - The only maintained tag for this image.


### Simple Curl
This Dockerfile is used for deploying via webhooks. It will automatically deploy to the `$DEVELOP_DEPLOY_LINK` env variable when `$CI_BRANCH` is `develop` and to the `$MASTER_DEPLOY_LINK` when `master`.

#### Use

```bash
docker pull thegraidenetwork/simple-curl[:tag]
```

#### Tags
- `latest` - The only maintained tag for this image.

## Development

New versions of each image are built automatically using Codeship whenever an update is made to the `master` branch of this repository. 

When you make changes to these Dockerfiles or add a new one, be sure to update the `CHANGELOG.md` and push a tag to our git repository.
