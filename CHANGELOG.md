# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- Nothing yet.

### Changed
- Nothing yet.

### Removed
- Nothing yet.

### Fixed
- Nothing yet.

## [0.1.0] - 2019-11-06

### Added
- `laravel-php` Dockerfile and base image.

### Removed
- `rancher-update` Dockerfile and base image.

## [0.0.1] - 2018-03-20

### Added
- Initial release of PHP-FPM-MySQL and Simple-Curl Dockerfiles/images.
- Setting up automated Codeship builds.
